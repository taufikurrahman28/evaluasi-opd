@extends('template.master')
@section('title','User')
@section('content')
     <!-- DataTales User -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data User</h6>
        </div>
        <div class="card-body">
            <a href="{{ route('user.create') }}" class="btn btn-primary mb-3">Tambah User</a>
            @if (session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @endif
            <div class="table-responsive">
                <table id="myTable" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($user as $row )
                        <tr>
                            <th>{{ $loop->iteration }}</th>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->email }}</td>
                            <td>
                                <form action="{{ route('user.password', $row->id) }}" style="display: inline-block;" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button type="submit" class="btn btn-danger btn-sm" style="background: rgb(168, 0, 0)" title="Reset Password">Reset Password</button>
                                </form>
                                <a href="{{ route('user.edit', $row->id) }}"class="btn btn-warning btn-sm">Edit</a>
                                <form action="{{ route('user.destroy', $row->id) }}" method="POST" style="display: inline-block;">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus data ini?')">Hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
<script>let table = new DataTable('#myTable');</script>
@endpush

