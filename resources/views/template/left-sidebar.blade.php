<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-10">
            <img src="{{ asset('logo-riau.png') }}" style="widht:25px; height:50px;">
            {{-- <i class="fas fa-laugh-wink"></i> --}}
        </div>
        <div>
            <div class="sidebar-brand-text-center mx-2" style="font-size: 12px">Sistem Monitoring dan Evaluasi Kominfo</div>
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @yield('dashboard')">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item @yield('kategori')">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
            aria-controls="collapsePages">
            <i class='bx bx-list-check' style="font-size: 25px"></i>
            <span>Menu</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('periodik.index') }}">
                    <span>Periode</span></a>

                <a class="collapse-item" href="{{ route('opd.index') }}">
                    <span>Opd</span></a>

                <a class="collapse-item" href="{{ route('kategori.index') }}">
                    <span>Kategori</span></a>

                <a class="collapse-item" href="{{ route('sub_kategori.index') }}">
                    <span>Sub Kategori</span></a>

                <a class="collapse-item" href="{{ route('evaluasi.index') }}">
                    <span>Evaluasi</span></a>
                <div class="collapse-divider"></div>
            </div>
        </div>
    </li>

    <li class="nav-item @yield('user')">
        <a class="nav-link" href="{{ route('user.index') }}">
            <i class="fas fa-user"></i>
            <span>User</span></a>
    </li>


    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
