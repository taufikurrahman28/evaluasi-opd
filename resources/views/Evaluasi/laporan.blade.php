<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Laporan Evaluasi</title>
    <style>
        #title {
            text-align: center;
        }

        #tabel-header {
            padding-bottom: 40px;
            border-bottom: 3px dashed black;
        }
        .tabel-data td  {
        border: 1px solid;
        }
        .tabel-data th {
        border: 1px solid;
        }
        .tabel-data {
            width: 100%;
            border-collapse: collapse;
            text-align: center;
        }
    </style>
</head>

<body>
    <header>
        <h3 id="title">Laporan <br> Pemantauan Website OPD</h3>
        <div id="data-header">
            <div id="tabel-header">
                <table>
                    <tr>
                        <td style="width:180px;">Tanggal</td>
                        <td>:</td>
                        <td>{{ $evaluasi->tanggal_pemantauan->format('d-m-Y') }}</td>
                    </tr>
                    <tr>
                        <td>OPD</td>
                        <td>:</td>
                        <td>{{ $evaluasi->opd->nama }}</td>
                    </tr>
                    <tr>
                        <td>Alamat Website</td>
                        <td>:</td>
                        <td>{{ $evaluasi->opd->link }}</td>
                    </tr>
                    <tr>
                        <td>Browser Pengujian</td>
                        <td>:</td>
                        <td>{{ $evaluasi->browser_pengujian }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </header>
    <main style="padding-top: 25px;">
        <h3 style="text-align: center;">Hasil Monitoring</h3>
        <div id="gambar" style="text-align: center">
             @foreach ($evaluasi->gambar as $image)
                <img style="width:90%; height: 60%;" src="{{ $image->path }}" alt="">
                <p style="font-size: 14px;">{{ $image->caption }}</p>
             @endforeach
        </div>
        <div>
            Ketersediaan fitur berdasarkan Pergub No. 11 Tahun 2018:
            <table class="tabel-data">
                <thead>
                    <tr>
                        <td style="width:25%;">Fitur</td>
                        <td>Ketersediaan</td>
                        <td>Keterangan</td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($kategoriUtama as $utama)
                        <tr>
                            <td>{{ $utama->sub_kategori->nama }}</td>
                            <td>{{ $utama->ketersediaan ? 'Ada' : 'Tidak ada' }}</td>
                            <td>{{ $utama->keterangan }}</td>
                        </tr>
                    @empty
                    <tr>
                        <td colspan="3">Tidak ada data</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            <div style="padding-top: 20px;">Ketersediaan fitur tambahan yang direkomendasikan:</div>
            <table class="tabel-data">
                <thead>
                    <tr>
                        <td style="width:25%;">Fitur</td>
                        <td>Ketersediaan</td>
                        <td>Keterangan</td>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($kategoriRekom as $rekom)
                        <tr>
                            <td>{{ $rekom->sub_kategori->nama }}</td>
                            <td>{{ $rekom->ketersediaan ? 'Ada' : 'Tidak ada' }}</td>
                            <td>{{ $rekom->keterangan }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td style="text-align: center" colspan="3">Tidak ada data</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div style="padding-top: 20px;">{!! $evaluasi->catatan !!}</div>
    </main>
</body>

</html>
