@extends('template.master')
@section('title', 'Edit Evaluasi')
@section('content')
    <form action="{{ route('evaluasi.update', $evaluasi->id) }}" id="form-evaluasi" method="post"
        enctype="multipart/form-data">
        @csrf
        @method('put')

        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Edit Evaluasi</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="opd_id">Nama OPD</label>
                                    <select class="form-control @error('opd_id') is-invalid @enderror" id="opd_id"
                                        name="opd_id">
                                        @foreach ($opd as $row)
                                        <option value="{{ $row->id }}" @selected($row->id==$evaluasi->opd_id)>{{ $row->nama}}</option>
                                        @endforeach
                                    </select>
                                    @error('nama')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="browser_pengujian">Browser Pengujian</label>
                                    <input type="text" class="form-control @error('browser_pengujian') is-invalid @enderror"
                                        id="browser_pengujian" name="browser_pengujian" value="{{ old('browser_pengujian', $evaluasi->browser_pengujian) }}"
                                        required>
                                    @error('browser_pengujian')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="tanggal_pemantauan">Tanggal</label>
                                    <input type="date"
                                        class="form-control @error('tanggal_pemantauan') is-invalid @enderror"
                                        id="tanggal_pemantauan" name="tanggal_pemantauan"
                                        value="{{ old('tanggal_pemantauan', $evaluasi->tanggal_pemantauan->format('Y-m-d')) }}"
                                        required>
                                    @error('tanggal_pemantauan')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <table id="tabel-gambar" class="table table-responsive w-full">
                                <thead>
                                    <tr>
                                        <td style="width:30%">Gambar</td>
                                        <td style="width: 70%">Caption</td>
                                        {{-- <td style="width: 5%">Action</td> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($evaluasi->gambar as $gambar)
                                    <tr class="tr-gambar" id="row-gambar{{ $loop->index+1 }}">
                                        <td>
                                            <div class="custom-file">
                                                <img src="{{ url($gambar->path) }}" alt="{{ $gambar->caption }}" class="img-fluid mb-3">
                                                <input type="file"
                                                    class="form-control-file input-gambar @error('gambar') is-invalid @enderror"
                                                    id="gambar1" name="gambar[{{ $gambar->id }}]" accept="image/png, image/jpg, image/jpeg"
                                                    value="{{ old('gambar') }}">
                                                @error('gambar')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text"
                                                    class="form-control input-caption @error('caption') is-invalid @enderror"
                                                    id="caption1" name="caption[]" value="{{ $gambar->caption }}" required>
                                                @error('caption')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </td>
                                        {{-- <td>
                                            @if ($loop->last)
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="button"
                                                    onclick="tambahGambar()">Tambah</button>
                                            </div>
                                            @endif
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            {{-- <div class="col-12">
                                <div class="form-group">
                                    <label for="kategori_id">Kategori</label>
                                    <select class="form-control @error('kategori_id') is-invalid @enderror" id="kategori_id"
                                        name="kategori_id" onchange="showTabel()" required>
                                        @foreach ($kategori as $row)
                                            <option selected disabled>-- Pilih Kategori --</option>
                                            <option value="{{ $row->id }}">{{ $row->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div> --}}

                            @foreach ($kategori as $item)
                                <table id="kategori" class="table">
                                    <thead>
                                        <tr>
                                            <td>Kategori</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="sub" id="row1">
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="kategori[]"
                                                        value="{{ $item->nama }}" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table id="sub" class="table table-responsive w-full">
                                    <thead>
                                        <tr>
                                            <td style="width:30%">Sub Kategori</td>
                                            <td style="width:10%">Ketersediaan</td>
                                            <td style="width:50%">Keterangan</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($detailEvaluasi->where('kategori_id', $item->id) as $data)
                                            <tr class="tr-sub" id="row1">
                                                <td>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control"
                                                            value="{{ $data->sub_kategori->nama }}" readonly>
                                                            <input type="hidden" class="form-control" name="sub_kategori_id[]"
                                                            value="{{ $data->sub_kategori_id }}" readonly>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <select
                                                            class="form-control input-ketersediaan @error('ketersediaan') is-invalid @enderror"
                                                            id="ketersediaan" name="ketersediaan[]" required>
                                                            <option value="" disabled>-- Pilih Ketersediaan
                                                                --
                                                            </option>
                                                            <option value="1" @selected($data->ketersediaan == 1)>Ada</option>
                                                            <option value="0" @selected($data->ketersediaan == 0)>Tidak Ada</option>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="text"
                                                            class="form-control input-keterangan @error('keterangan') is-invalid @enderror"
                                                            id="keterangan" name="keterangan[]" value="{{ $data->keterangan }}">
                                                        @error('keterangan')
                                                            <div class="invalid-feedback">
                                                                {{ $message }}
                                                            </div>
                                                        @enderror
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endforeach

                            @foreach ($kategoriTanpaSub as $item)
                                <table id="kategori" class="table">
                                    <thead>
                                        <tr>
                                            <td>Kategori</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="sub" id="row1">
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="kategori[]"
                                                        value="{{ $item->nama }}" readonly>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endforeach




                            <div class="col-12">
                                <label for="gambar">Catatan</label>
                                <div id="editor" style="height: 200px;">
                                </div>
                                <input type="hidden" name="catatan">
                                @error('catatan')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@push('style')
    <link href="//cdn.quilljs.com/1.3.6/quill.core.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
@endpush

@push('script')
    <script src="//cdn.quilljs.com/1.3.6/quill.core.js"></script>
    <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>

    <script>
        let quill = new Quill('#editor', {
            modules: {
                toolbar: [
                    [{
                        header: [1, 2, false]
                    }],
                    ['bold', 'italic', 'underline', 'strike'],
                    [{
                        'list': 'ordered'
                    }, {
                        'list': 'bullet'
                    }]
                ]
            },
            theme: 'snow'
        });

        quill.root.innerHTML = {!! json_encode($evaluasi->catatan) !!};

        var form = document.querySelector('#form-evaluasi');
        form.onsubmit = function() {
            var catatan = document.querySelector('input[name=catatan]');
            catatan.value = quill.root.innerHTML;
        };
    </script>

    <script>
        //global variabel
        let lastRowId = 1;
        let lastGambarId = 1;
        const tabel = document.getElementById('sub');
        const tabelGambar = document.getElementById('tabel-gambar');

        //get sub kategori
        const getSubKategori = async () => {
            lastRowId++;
            // const kategoriId = document.getElementById('kategori_id').value;
            let objectSubKategori = await fetch(`/evaluasi/getSubKategori/`);
            let response = await objectSubKategori.json();
            response.map((e) => {
                const subKategori = document.getElementById(`sub_kategori_id-${lastRowId}`);
                const option = document.createElement("option");
                option.text = e.nama;
                option.value = e.id;
                subKategori.add(option);
            })
        }

        //tampilkan tabel dan get subcategori ketika kategori dipilih
        // const showTabel = () => {
        //     tabel.classList.remove("d-none");
        //     getSubKategori();
        // };


        //ambil semua nilai select sub kategori
        // const selectedSubKategori = [];
        // const addSubKategori = () => {
        //     const subKategoriSelect = document.getElementById(`sub_kategori_id-${lastRowId}`)
        //     const valueSubKategori = subKategoriSelect.value;
        //     selectedSubKategori.push(valueSubKategori);
        //     console.log(selectedSubKategori);
        // }

        //tambah form subkategori
        const tambahSub = () => {

            getSubKategori();
            var row = tabel.insertRow(-1);
            row.id = "row" + lastRowId;
            row.className = "tr-sub";
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            var cell4 = row.insertCell(3);
            cell1.innerHTML =
                `<div class="form-group"> <select class="form-control input-sub @error('sub_kategori_id') is-invalid @enderror"name="sub_kategori_id[]" id="sub_kategori_id-${lastRowId}" required><option value="" selected disabled >-- Pilih Sub Kategori --</option></select></div>`;
            cell2.innerHTML =
                `<div class="form-group"><select class="form-control input-ketersediaan @error('ketersediaan') is-invalid @enderror"id="ketersediaan" name="ketersediaan[]" required><option value="" selected disabled >-- Pilih Ketersediaan --</option><option value="1">Ada</option><option value="0">Tidak Ada</option></select></div>`;
            cell3.innerHTML =
                `<div class="form-group"><input type="text"class="form-control input-keterangan @error('keterangan') is-invalid @enderror"id="keterangan" name="keterangan[]" required>@error('keterangan')<div class="invalid-feedback">{{ $message }}</div>@enderror</div>`;
            cell4.innerHTML =
                `<div class="form-group"><button class="btn btn-danger" type="button"onclick="hapusSub(${lastRowId})">Hapus</button></div>`;

        };

        const tambahGambar = () => {
            lastGambarId++;
            var row = tabelGambar.insertRow(-1);
            row.id = "row-gambar" + lastGambarId;
            row.className = "tr-gambar";
            var cell1 = row.insertCell(0);
            var cell2 = row.insertCell(1);
            var cell3 = row.insertCell(2);
            cell1.innerHTML =
                `<div class="custom-file"><input type="file"class="form-control-file input-gambar @error('gambar') is-invalid @enderror"id="gambar${lastGambarId}" name="gambar[]" accept="image/png, image/jpg, image/jpeg" required>@error('gambar')<div class="invalid-feedback">{{ $message }}</div>@enderror</div>`;
            cell2.innerHTML =
                `<div class="form-group"><input type="text" class="form-control input-caption @error('caption') is-invalid @enderror"id="caption${lastGambarId}" name="caption[]" required>@error('caption')<div class="invalid-feedback">{{ $message }}</div>@enderror</div>`;
            cell3.innerHTML =
                `<div class="form-group"><button class="btn btn-danger" type="button"onclick="hapusGambar(${lastGambarId})">Hapus</button></div>`;
        };

        const hapusGambar = (rowId) => {
            var row = document.getElementById('row-gambar' + rowId);
            row.parentNode.removeChild(row);

        };

        //hapus form subkategori tambahan
        const hapusSub = (rowId) => {
            var row = document.getElementById('row' + rowId);
            row.parentNode.removeChild(row);
        };

        // const handleSubmit = () => {
        //     const formEvaluasi = document.getElementById('form-evaluasi');
        //     const uniqueArr = selectedSubKategori.filter(function(value, index, self) {
        //         return self.indexOf(value) === index;
        //     });

        //     if (uniqueArr.length !== selectedSubKategori.length) {
        //         alert("Nilai duplikat ditemukan!");
        //     } else {
        //         formEvaluasi.submit();
        //     }
        // };
    </script>
@endpush
