<?php

namespace Database\Seeders;

use App\Models\SubKategori;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SubKategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubKategori::create([
            'nama' => 'Sejarah',
            'kategori_id' => 1,
            'status_tampil' => 1
        ]);

        SubKategori::create([
            'nama' => 'Halaman Utama',
            'kategori_id' => 1,
            'status_tampil' => 1
        ]);
    }
}
