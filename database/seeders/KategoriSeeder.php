<?php

namespace Database\Seeders;

use App\Models\Kategori;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kategori::create([
            'nama' => 'Fitur Utama',
            'status_tampil' => '1',
            'jenis_sumber' => 'utama',
            'periodik_id' => 1
        ]);

        Kategori::create([
            'nama' => 'Fitur Rekomendasi',
            'status_tampil' => '1',
            'jenis_sumber' => 'rekomendasi',
            'periodik_id' => 1
        ]);
    }
}
