<?php

namespace Database\Seeders;

use App\Models\Periodik;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PeriodikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Periodik::create([
            'nama' => 'Periode 1 (Januari)',
            'mulai' => '2023-01-01',
            'selesai' => '2023-06-01',
            'is_aktif' => 1
        ]);
    }
}
