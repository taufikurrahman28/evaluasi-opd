<?php

namespace Database\Seeders;

use App\Models\Opd;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class OpdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $opdData = [
            ['nama' => 'Badan Kepegawaian Daerah','link' => 'https://bkd.riau.go.id/'],
            ['nama' => 'Badan Pengelola Keauangan dan Aset Daerah','link' => '	https://bpkad.riau.go.id/']
        ];

        foreach ($opdData as $data) {
            Opd::create($data);
        }
        // Opd::create([
        //     'nama' => 'Badan Kepegawaian Daerah',
        //     'link' => '	https://bkd.riau.go.id/',
        // ]);

        // Opd::create([
        //     'nama' => 'Badan Pengelola Keauangan dan Aset Daerah',
        //     'link' => '	https://bpkad.riau.go.id/',
        // ]);
    }
}
