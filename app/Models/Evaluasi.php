<?php

namespace App\Models;

use App\Models\Periodik;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Evaluasi extends Model
{
    use HasFactory;
    protected $table = 'evaluasi';

    protected $fillable = [
        'nama_opd',
        'nama_web',
        'browser_pengujian',
        'tanggal_pemantauan',
        'keterangan',
        'periodik_id',
        'kategori_id',
        'opd_id',
        'catatan',
    ];

    protected $dates = ['tanggal_pemantauan'];

    public function periodik()
    {
        return $this->belongsTo(Periodik::class);
    }

    public function opd()
    {
        return $this->belongsTo(Opd::class);
    }

    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }

    public function details()
    {
        return $this->hasMany(DetailEvaluasi::class);
    }

    public function gambar()
    {
        return $this->hasMany(GambarEvaluasi::class);
    }
}
