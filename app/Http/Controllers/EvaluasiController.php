<?php

namespace App\Http\Controllers;

use App\Models\DetailEvaluasi;
use App\Models\Evaluasi;
use App\Models\GambarEvaluasi;
use App\Models\Kategori;
use App\Models\Opd;
use App\Models\Periodik;
use App\Models\SubKategori;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;


class EvaluasiController extends Controller
{
    public function index()
    {
        $evaluasi = Evaluasi::with('details')->get();

        return view('Evaluasi.index', ['evaluasi' => $evaluasi]);
    }

    public function create()
    {
        $kategori = Kategori::has('sub_kategori')->where('status_tampil', 1)->get();
        $kategoriTanpaSub = Kategori::whereDoesntHave('sub_kategori')->where('status_tampil', 1)->get();
        $sub_kategori = SubKategori::get();
        $opd = Opd::get();

        return view('Evaluasi.create', ['kategori' => $kategori, 'sub_kategori' => $sub_kategori, 'kategoriTanpaSub' => $kategoriTanpaSub, 'opd' => $opd]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'opd_id' => 'required',
            'browser_pengujian' => 'required',
            'tanggal_pemantauan' => 'required',
            'sub_kategori_id.*' => 'required',
            'ketersediaan.*' => 'required',
            'keterangan.*' => 'nullable|string',
            'catatan' => 'required',
            'gambar' => 'required',
            'gambar.*' => 'image|mimes:jpeg,png,jpg|max:2048',
            'caption.*' => 'required',
        ]);


        Evaluasi::create([
            'opd_id' => $validatedData['opd_id'],
            'browser_pengujian' => $validatedData['browser_pengujian'],
            'tanggal_pemantauan' => $validatedData['tanggal_pemantauan'],
            'catatan' => $validatedData['catatan'],
            'periodik_id' => Periodik::where('is_aktif', 1)->first()->id,
        ]);

        $evaluasi = Evaluasi::latest()->first();



        for ($i = 0; $i < count($validatedData['sub_kategori_id']); $i++) {
            $data = [
                'evaluasi_id' => $evaluasi->id,
                'sub_kategori_id' => $validatedData['sub_kategori_id'][$i],
                'ketersediaan' => $validatedData['ketersediaan'][$i],
                'keterangan' => $validatedData['keterangan'][$i],
            ];
            DetailEvaluasi::create($data);
        }

        foreach ($request->file('gambar') as $key => $file) {
            $path = $file->store('public/gambar-evaluasi');
            $nama_file = $file->getClientOriginalName();
            $caption = $validatedData['caption'][$key];
            $path_public = 'storage/gambar-evaluasi/' . $file->hashName();
            $gambar = new GambarEvaluasi;
            $gambar->nama = $nama_file;
            $gambar->caption = $caption;
            $gambar->path = $path_public;
            $gambar->evaluasi_id = $evaluasi->id;
            $gambar->save();
        }

        return to_route('evaluasi.index')->with('success', 'Data berhasil ditambahkan');
    }

    public function getSubKategori()
    {
        $dataSub = SubKategori::get();

        return response()->json($dataSub);
    }

    public function printPDF($id)
    {
        $evaluasi = Evaluasi::findOrFail($id);
        $kategoriUtama = $evaluasi->details()->whereHas('sub_kategori', function ($query) {
            $query->where('kategori_id', 1);
        })->get();
        $kategoriRekom = $evaluasi->details()->whereHas('sub_kategori', function ($query) {
            $query->where('kategori_id', 2);
        })->get();
        $details = $evaluasi->details;
        foreach ($details as $detail) {
            $kategori = $detail->sub_kategori->kategori;
        }
        ;
        $pdf = Pdf::loadView('Evaluasi.laporan', [
            'evaluasi' => $evaluasi,
            'kategori' => $kategori,
            'kategoriUtama' => $kategoriUtama,
            'kategoriRekom' => $kategoriRekom,
        ]);
        return $pdf->stream('Laporan Evaluasi.pdf');
    }

    public function edit($id)
    {
        $evaluasi = Evaluasi::with('gambar')->find($id);
        $kategori = Kategori::has('sub_kategori')->where('status_tampil', 1)->where('periodik_id', $evaluasi->periodik_id)->get();
        $kategoriTanpaSub = Kategori::whereDoesntHave('sub_kategori')->where('status_tampil', 1)->get();
        $sub_kategori = SubKategori::get();
        $opd = Opd::get();
        $gambar_evaluasi = GambarEvaluasi::get();

        // $detailEvaluasi = DetailEvaluasi::with(['kategori', 'sub_kategori'])->where('evaluasi_id', $evaluasi->id)->get();
        $detailEvaluasi = DetailEvaluasi::with([
            'sub_kategori.kategori' => function ($query) {
                $query->select('id');
            }
        ])
            ->select('sub_kategori_id', 'evaluasi_id', 'ketersediaan', 'keterangan')
            ->where('evaluasi_id', $evaluasi->id)
            ->get()
            ->map(function ($detail) {
                $detail['kategori_id'] = optional($detail->sub_kategori)->kategori->id;
                return $detail;
            });

        return view('Evaluasi.edit', [
            'evaluasi' => $evaluasi,
            'kategori' => $kategori,
            'sub_kategori' => $sub_kategori,
            'kategoriTanpaSub' => $kategoriTanpaSub,
            'opd' => $opd,
            'gambar_evaluasi' => $gambar_evaluasi,
            'detailEvaluasi' => $detailEvaluasi,
        ]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'opd_id' => 'required',
            'browser_pengujian' => 'required',
            'tanggal_pemantauan' => 'required',
            'catatan' => 'required',
        ]);

        $data = $request->all();

        $evaluasi = Evaluasi::with('gambar')->find($id);
        $detailEvaluasi = DetailEvaluasi::where('evaluasi_id', $evaluasi->id)->get();

        $evaluasi->update([
            'opd_id' => $data['opd_id'],
            'browser_pengujian' => $data['browser_pengujian'],
            'tanggal_pemantauan' => $data['tanggal_pemantauan'],
            'catatan' => $data['catatan'],
        ]);

        for ($i = 0; $i < $detailEvaluasi->count(); $i++) {
            $detailEvaluasi[$i]->update([
                'sub_kategori_id' => $data['sub_kategori_id'][$i],
                'ketersediaan' => $data['ketersediaan'][$i],
                'keterangan' => $data['keterangan'][$i],
            ]);
        }

        if ($request->has('gambar')) {
            $indexes = array_keys($request->gambar);


            for ($i = 0; $i < count($indexes); $i++) {
                $index = $indexes[$i];
                $file = $request->gambar[$index];


                $path = $file->store('public/gambar-evaluasi');
                $nama_file = $file->getClientOriginalName();
                $path_public = 'storage/gambar-evaluasi/' . $file->hashName();

                $gambarEvaluasi = GambarEvaluasi::where('id', $indexes[$i])->first();

                unlink($gambarEvaluasi->path);

                $gambarEvaluasi->update([
                    'nama' => $nama_file,
                    'path' => $path_public,
                    'evaluasi_id' => $evaluasi->id,
                ]);
            }
        }

        return redirect()->route('evaluasi.index');
    }
}
