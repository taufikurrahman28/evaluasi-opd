<?php

namespace App\Http\Controllers;

use App\Models\Opd;
use Illuminate\Http\Request;

class OpdController extends Controller
{
    public function index()
    {
        $opd = Opd::get();

        return view('Opd.index', ['opd' => $opd]);
    }

    public function create()
    {
        return view('Opd.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validateData = $request->validate([
            'nama' => 'required|unique:opd|',
            'link' => 'required',
        ], [
                'required' => ':attribute tidak boleh kosong',
                'unique' => ':attribute sudah dipakai',
            ], [
                'nama' => 'Nama',
                'link' => 'Link',
            ]);
        Opd::create($validateData);
        return redirect()->route('opd.index');
    }

    public function edit($id)
    {
        $opd = Opd::find($id);
        // dd($kategori)->all();
        return view('opd.edit', ['opd' => $opd]);
    }

    public function update(Request $request, $id)
    {
        $opd = Opd::find($id);
        $validateData = $request->validate([
            'nama' => 'required|unique:opd,nama,' . $id,
            'link' => 'required',
        ], [
                'required' => ':attribute tidak boleh kosong',
                'unique' => ':attribute sudah dipakai',
            ], [
                'nama' => 'Nama',
                'link' => 'Link',
            ]);
        $opd->update($validateData);
        return redirect()->route('opd.index');
    }
    public function destroy($id)
    {
        $opd = Opd::find($id);
        $opd->delete();
        return redirect()->route('opd.index');
    }
}
